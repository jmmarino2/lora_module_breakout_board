# LoRa_Module_Breakout_Board

Breakout board for LoRa modules RFM9x/HopeRF/Ai-Thinker

SMA/U.FL edge connector has been taken from https://github.com/adafruit/Adafruit-RFM-LoRa-Radio-Breakout-PCB
